package com.itdfq.springboot_shrio.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author GocChin
 * @Date 2021/6/6 16:46
 * @Blog: itdfq.com
 * @QQ: 909256107
 * @Descript:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Users {
    private int id;
    private String name;
    private String pwd;
    private String perms;
    private String role;
}
