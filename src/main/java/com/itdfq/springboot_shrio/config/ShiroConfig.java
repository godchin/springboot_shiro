package com.itdfq.springboot_shrio.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author GocChin
 * @Date 2021/6/6 15:28
 * @Blog: itdfq.com
 * @QQ: 909256107
 * @Descript:
 */
@Configuration
public class ShiroConfig {

    //ShiroFilterFacctoryBean  3
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("DefaultWebSecurityManager") DefaultWebSecurityManager manager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        //设置安全管理器
        bean.setSecurityManager(manager);
        //添加shiro的内置过滤器
        /**
         * anon:无需认证就可以访问
         * authc:必须认证才能用
         * user:必须拥有记住我功能才能使用
         * perms:拥有对某个资源的权限才能访问
         * role:拥有某个角色权限才能访问
         */
        //拦截
        Map<String,String> filterMap = new LinkedHashMap<>();
        //授权 ,没有权限会跳转到位置页面
        filterMap.put("/user/add","perms[user:add]");
        filterMap.put("/user/update","perms[user:update]");


//        filterMap.put("/user/add","authc");
//        filterMap.put("/user/update","authc");
//        上面两种写法等效于   可以采用通配符
        filterMap.put("/user/*","authc");

        bean.setFilterChainDefinitionMap(filterMap);

        //设置未登录页面
        bean.setLoginUrl("/tologin");
        //设置未授权页面
        bean.setUnauthorizedUrl("/noauth");
        return bean;

    }

    //DafaultWebSecurityManager 2
    @Bean(name = "DefaultWebSecurityManager")                                                       //Qualifier  通过这个注解获取spring管理的 userRealm()方法
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        return securityManager;
    }

    //创建realm对象，需要自定义类  1
    @Bean(name = "userRealm")
    public UserRealm userRealm(){
        UserRealm userRealm = new UserRealm();
        return new UserRealm();
    }


    //整合ShiroDialect:用于整合Thymeleaf和shiro
    @Bean
    public ShiroDialect getShiroDialect(){
        return new ShiroDialect();
    }





}
