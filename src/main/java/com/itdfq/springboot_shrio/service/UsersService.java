package com.itdfq.springboot_shrio.service;

import com.itdfq.springboot_shrio.pojo.Users;

/**
 * @Author GocChin
 * @Date 2021/6/6 16:55
 * @Blog: itdfq.com
 * @QQ: 909256107
 * @Descript:
 */
public interface UsersService {
     Users findByUsername(String username);


}
