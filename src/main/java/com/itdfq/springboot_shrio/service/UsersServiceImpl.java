package com.itdfq.springboot_shrio.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itdfq.springboot_shrio.dao.UsersMapper;
import com.itdfq.springboot_shrio.pojo.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 * @Author GocChin
 * @Date 2021/6/6 16:49
 * @Blog: itdfq.com
 * @QQ: 909256107
 * @Descript:
 */
@Service
public class UsersServiceImpl implements UsersService{

    @Autowired
    private UsersMapper usersMapper;

    private QueryWrapper<Users>  wrapper  = new QueryWrapper();


    public Users findByUsername(String username){
        wrapper.clear();
        wrapper.eq("name",username);
     return    usersMapper.selectOne(wrapper);
    }
}
