package com.itdfq.springboot_shrio.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itdfq.springboot_shrio.pojo.Users;

/**
 * @Author GocChin
 * @Date 2021/6/6 16:48
 * @Blog: itdfq.com
 * @QQ: 909256107
 * @Descript:
 */
public interface UsersMapper extends BaseMapper<Users> {
}
