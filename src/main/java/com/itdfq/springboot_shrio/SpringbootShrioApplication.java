package com.itdfq.springboot_shrio;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.itdfq.springboot_shrio.dao")
@SpringBootApplication
public class SpringbootShrioApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootShrioApplication.class, args);
    }
}
