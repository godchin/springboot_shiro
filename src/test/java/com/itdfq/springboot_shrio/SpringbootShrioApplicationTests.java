package com.itdfq.springboot_shrio;

import com.itdfq.springboot_shrio.pojo.Users;
import com.itdfq.springboot_shrio.service.UsersService;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootShrioApplicationTests {

    @Autowired
    private UsersService usersService;

    //测试md5盐值加密 对123进行加密
    @Test
    void contextLoads() {
        //盐
        String Salt = new SecureRandomNumberGenerator().nextBytes().toHex();
                                                       //加密格式     加密密码     盐值     加密次数
        SimpleHash simpleHash = new SimpleHash("md5","123","root",1024);
        String NewPassword = simpleHash.toString();
        System.out.println("NewPassword"+NewPassword); //a9376b751f778ce53d8c5049a85c95c7
        System.out.println("Salt:"+Salt);  //c7df5e4f1c07ce67062c80ffac189198
    }

    @Test
    public void jiemi(){

    }



}
